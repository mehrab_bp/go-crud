package models

import "gorm.io/gorm"

type Post struct {
    gorm.Model
    Text       string `json:"text"`
	UserId  string `json:"userId"`
}
