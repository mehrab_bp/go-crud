package posts

import (
    "gorm.io/gorm"
    "github.com/gin-gonic/gin"
)

type handler struct {
    DB *gorm.DB
}

func RegisterRoutes(router *gin.Engine, db *gorm.DB) {
    h := &handler{
        DB: db,
    }

    routes := router.Group("/posts")
    routes.POST("/", h.AddPost)
}

