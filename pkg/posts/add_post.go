package posts

import (
    "net/http"
    "packages/pkg/common/models"
    "github.com/gin-gonic/gin"
)

type AddPostRequestBody struct {
    Text       string `json:"text"`
    UserId     string `json:"userId"`
}

func (h handler) AddPost(ctx *gin.Context) {
    body := AddPostRequestBody{}

    if err := ctx.BindJSON(&body); err != nil {
        ctx.AbortWithError(http.StatusBadRequest, err)
        return
    }

    var post models.Post

    post.Text = body.Text
    post.UserId = body.UserId


    if result := h.DB.Create(&post); result.Error != nil {
        ctx.AbortWithError(http.StatusNotFound, result.Error)
        return
    }

    ctx.JSON(http.StatusCreated, &post)
}
