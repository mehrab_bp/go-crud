package users

import (
    "net/http"
    "packages/pkg/common/models"
    "github.com/gin-gonic/gin"
)

type AddUserRequestBody struct {
    Name       string `json:"name"`
    Phone      string `json:"phone"`
    Email 	   string `json:"email"`
	Id 	   	   string `json:"id"`
}

func (h handler) AddUser(ctx *gin.Context) {
    body := AddUserRequestBody{}

    if err := ctx.BindJSON(&body); err != nil {
        ctx.AbortWithError(http.StatusBadRequest, err)
        return
    }

    var user models.User

    user.Name = body.Name
    user.Phone = body.Phone
    user.Email = body.Email
	user.Id = body.Id

    if result := h.DB.Create(&user); result.Error != nil {
        ctx.AbortWithError(http.StatusNotFound, result.Error)
        return
    }

    ctx.JSON(http.StatusCreated, &user)
}
