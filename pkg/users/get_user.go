package users

import (
    "net/http"

    "packages/pkg/common/models"
    "github.com/gin-gonic/gin"
)

func (h handler) GetUsers(ctx *gin.Context) {
    var users []models.User

    if result := h.DB.Preload("Posts").Find(&users); result.Error != nil {
        ctx.AbortWithError(http.StatusNotFound, result.Error)
        return
    }

    ctx.JSON(http.StatusOK, &users)
}

func (h handler) GetUser(ctx *gin.Context) {
    id := ctx.Param("id")

    var user models.User

    if result := h.DB.Preload("Posts").Find(&user, "id = ?", id); result.Error != nil {
        ctx.AbortWithError(http.StatusNotFound, result.Error)
        return
    }

    ctx.JSON(http.StatusOK, &user)
}