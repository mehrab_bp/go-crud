package users

import (
    "net/http"

    "packages/pkg/common/models"
    "github.com/gin-gonic/gin"
)

type UpdateUserRequestBody struct {
    Name       string `json:"name"`
    Phone      string `json:"phone"`
    Email 	   string `json:"email"`
	Id 	   	   string `json:"id"`
}

func (h handler) UpdateUser(ctx *gin.Context) {
    id := ctx.Param("id")
    body := UpdateUserRequestBody{}

    if err := ctx.BindJSON(&body); err != nil {
        ctx.AbortWithError(http.StatusBadRequest, err)
        return
    }

    var user models.User

    if result := h.DB.First(&user, id); result.Error != nil {
        ctx.AbortWithError(http.StatusNotFound, result.Error)
        return
    }

    user.Name = body.Name
    user.Phone = body.Phone
    user.Email = body.Email

    h.DB.Save(&user)

    ctx.JSON(http.StatusOK, &user)
}
