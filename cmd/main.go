package main

import (
    "packages/pkg/users"
    "packages/pkg/posts"
    "packages/pkg/common/db"
    "github.com/gin-gonic/gin"
)

func main() {

    port := "0.0.0.0:8080"
	dbUrl := "postgres://postgres:1234@127.0.0.1:5432/go-test"

    router := gin.Default()
    dbHandler := db.Init(dbUrl)

    users.RegisterRoutes(router, dbHandler)
    posts.RegisterRoutes(router, dbHandler)

    router.GET("/", func(ctx *gin.Context) {
        ctx.JSON(200, gin.H{
            "port":  port,
            "dbUrl": dbUrl,
        })
    })

    router.Run(port)
}
